import React, {useState} from "react";
import {Card, CardHeader, CardMedia, Grid, makeStyles, CardActionArea, Button} from "@material-ui/core";
import {apiURL} from "../../config";
import {Link} from "react-router-dom";
import Modal from "../../components/PhotoModal/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {deletePhotosRequest} from "../../store/actions/photosActions";

const useStyles = makeStyles({
    card: {
        height: "100%",
        border: "1px solid black",
    },
    media: {
        height: 0,
        paddingTop: "56.25%",
    },
    user: {
        fontSize: "20px",
        padding: "10px",
        margin: "10px",
        color: "black",
    },
});

const Photo = ({id, title, image, author, idGallery}) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const user = useSelector((state) => state.users.user);

    const [modal, setModal] = useState(false);

    const openModal = () => {
        setModal(true);
    };

    const closeModal = () => {
        setModal(false);
    };

    return (
        <Grid item xs sm md={6} lg={4}>
            <Card className={classes.card}>
                <CardActionArea>
                    <CardMedia
                        image={apiURL + "/" + image}
                        title={title}
                        className={classes.media}
                        onClick={openModal}
                    />
                </CardActionArea>
                <CardHeader title={title}/>
                {!idGallery && (
                    <Link to={"/user/" + author._id} className={classes.user}>
                        By: {author.displayName}
                    </Link>
                )}
                {id && user && author._id === user._id && (
                    <Button
                        variant="outlined"
                        onClick={() => dispatch(deletePhotosRequest({id, idGallery}))}
                        style={{margin: "10px"}}
                    >
                        Delete
                    </Button>
                )}
            </Card>
            <Modal show={modal} closed={closeModal} image={apiURL + "/" + image} title={title}/>
        </Grid>
    );
};

export default Photo;
