import React, {useState} from "react";
import {Button, Grid, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import FileInput from "../../components/UI/Form/FileInput";
import {postPhotosRequest} from "../../store/actions/photosActions";
import {useDispatch} from "react-redux";

const AddNewPhoto = () => {
    const dispatch = useDispatch();
    const [photo, setPhoto] = useState({
        title: "",
        image: "",
    });

    const inputChangeHandler = (e) => {
        const {name, value} = e.target;

        setPhoto((prev) => ({...prev, [name]: value}));
    };

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];

        setPhoto((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    const submitFormHandler = (e) => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(photo).forEach((key) => {
            formData.append(key, photo[key]);
        });

        dispatch(postPhotosRequest(formData));
    };

    return (
        <Grid container direction="column" spacing={2} component="form" onSubmit={submitFormHandler}>
            <Typography variant="h4">Add New Photo</Typography>
            <Grid item>
                <FormElement label="Title" name="title" value={photo.title} onChange={inputChangeHandler} required/>
            </Grid>
            <Grid item>
                <FileInput label="Image" name="image" onChange={fileChangeHandler} required/>
            </Grid>
            <Grid item>
                <Button type="submit" variant="outlined" color="secondary">
                    Add
                </Button>
            </Grid>
        </Grid>
    );
};

export default AddNewPhoto;
