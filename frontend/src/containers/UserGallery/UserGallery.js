import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotosRequest, fetchUserPhotosRequest} from "../../store/actions/photosActions";
import {Grid, makeStyles, Typography} from "@material-ui/core";
import Photo from "../AllPhoto/Photo";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
    link: {
        textDecoration: "none",
        color: "black",
        border: "1px solid black",
        padding: "10px",
    },
    empty: {margin: "150px auto"},
});

const UserGallery = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const idGallery = props.match.params.id;
    const user = useSelector((state) => state.users.user);

    useEffect(() => {
        dispatch(fetchPhotosRequest(idGallery));
        dispatch(fetchUserPhotosRequest(idGallery));
    }, [dispatch, idGallery]);

    const photos = useSelector((state) => state.photos.photos);
    const userGallery = useSelector((state) => state.photos.userGallery);

    return (
        <Grid container spacing={2} direction="column">
            <Grid item container alignItems="center" justifyContent="space-between">
                <Typography variant="h3">{userGallery.displayName} Gallery</Typography>
                {user && idGallery === user._id && (
                    <Link className={classes.link} to="/add-photo">
                        Add New Photo
                    </Link>
                )}
            </Grid>
            <Grid item container spacing={2}>
                {photos.length > 0 ? (
                    photos.map((photo) => (
                        <Photo
                            key={photo._id}
                            id={photo._id}
                            image={photo.image}
                            title={photo.title}
                            author={photo.user}
                            idGallery={idGallery}
                        />
                    ))
                ) : (
                    <Typography className={classes.empty} variant="h5">
                        Gallery is empty
                    </Typography>
                )}
            </Grid>
        </Grid>
    );
};

export default UserGallery;
