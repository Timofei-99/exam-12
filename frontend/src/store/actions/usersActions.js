import usersSlice from "../slices/UsersSlice";

export const {
    registerRequest,
    registerSuccess,
    registerFailure,
    loginRequest,
    loginSuccess,
    loginFailure,
    logoutSuccess,
    facebookLoginRequest,
    googleLoginRequest,
    logoutRequest,
} = usersSlice.actions;
