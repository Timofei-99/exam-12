import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {Avatar, IconButton, makeStyles, Menu, MenuItem} from "@material-ui/core";
import {logoutRequest} from "../../../../store/actions/usersActions";
import {apiURL} from "../../../../config";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    avatar: {
        width: theme.spacing(4),
        height: theme.spacing(4),
    },
    link: {
        textDecoration: "none",
        color: "black",
    },
}));

const UserMenu = ({user}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <IconButton onClick={handleClick} color="inherit">
                {user.avatar ? (
                    <Avatar src={apiURL + "/" + user.avatar} className={classes.avatar}/>
                ) : (
                    <Avatar className={classes.avatar}/>
                )}
            </IconButton>
            <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
                <MenuItem disabled>{user.displayName}</MenuItem>
                <MenuItem>
                    <Link className={classes.link} to="/add-photo">
                        Add New Photo
                    </Link>
                </MenuItem>
                <MenuItem>
                    <Link className={classes.link} to={"/user/" + user._id}>
                        My gallery
                    </Link>
                </MenuItem>
                <MenuItem onClick={() => dispatch(logoutRequest())}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;
