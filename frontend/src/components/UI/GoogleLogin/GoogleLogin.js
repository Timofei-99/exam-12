import React from 'react';
import GoogleLoginButton from 'react-google-login';
import {googleClientId} from "../../../config";
import {Button} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {googleLoginRequest} from "../../../store/actions/usersActions";

const GoogleLogin = () => {
    const dispatch = useDispatch();
    const handleLogin = (response) => {
        console.log(response)
        dispatch(googleLoginRequest(response))
    }

    return (
        <GoogleLoginButton
            clientId={googleClientId}
            render={(props) => (
                <Button
                    fullWidth
                    variant='outlined'
                    color='primary'
                    onClick={props.onClick}
                >
                    Log in with google
                </Button>
            )}
            onSuccess={handleLogin}
            cookiePolicy={'single_host_origin'}
        />
    );
};

export default GoogleLogin;