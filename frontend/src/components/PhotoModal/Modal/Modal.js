import React from "react";
import Backdrop from "../BackDrop/BackDrop";
import {Button, makeStyles, Grid} from "@material-ui/core";

const useStyles = makeStyles({
    Modal: {
        position: "fixed",
        zIndex: "500",
        backgroundColor: "white",
        width: "75%",
        border: "1px solid #ccc",
        boxShadow: "1px 1px 1px black",
        padding: "16px",
        left: "15%",
        top: "10%",
        boxSizing: "border-box",
        transition: "all 0.3s ease-out",
    },
    media: {
        height: "450px",
        width: "850px",
    },
});

const Modal = (props) => {
    const classes = useStyles();

    return (
        <>
            <Backdrop show={props.show} onClick={props.closed}/>
            <Grid
                container
                alignItems="center"
                direction="column"
                className={classes.Modal}
                style={{
                    transform: props.show ? "translateY(0)" : "translateY(-100vh)",
                    opacity: props.show ? "1" : "0",
                }}
            >
                <p>{props.title}</p>
                <img src={props.image} alt={props.image} className={classes.media}/>
                <Button onClick={props.closed}>Close</Button>
            </Grid>
        </>
    );
};

export default Modal;
