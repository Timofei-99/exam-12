const path = require("path");
const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, "public"),
    db: {
        url: "mongodb://localhost/exam-12",
    },
    google: {
        clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID
    }
};
